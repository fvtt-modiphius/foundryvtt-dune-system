export class DuneRoll {
  async performTest(dicePool, target, focus, usingDetermination, complicationRange, actor, flavorText) {
    let finalDicePool = dicePool + (!!usingDetermination ? -1 : 0);
    const _r = new Roll(finalDicePool + 'd20');
    let _focus = focus || 1;
    let _complication = complicationRange || 20;
    let diceString = '';
    let success = 0;
    let complication = 0;
    let targetText =  game.i18n.format('DUNE.roll.target') + ': ' + target;
    if (focus)
      targetText += ', ' + game.i18n.format('DUNE.roll.focus') + ': ' + _focus;
    if (complicationRange)
      targetText += ', ' + game.i18n.format('DUNE.roll.complicationFrom') + ': ' + _complication;
    if (usingDetermination)
      targetText += ', ' + game.i18n.format('DUNE.roll.usingDetermination');
    _r.roll().then(dr => {
      for (let i = 0; i < finalDicePool; i++) {
        let result = dr.dice[0].results[i].result;
        if (result <= _focus) {
          diceString += '<li class="roll die d20 max">' + result + '</li>';
          success += 2;
        }
        else if (result <= target) {
          diceString += '<li class="roll die d20">' + result + '</li>';
          success += 1;
        }
        else if (result >= _complication) {
          diceString += '<li class="roll die d20 min">' + result + '</li>';
          complication += 1;
        }
        else {
          diceString += '<li class="roll die d20">' + result + '</li>';
        }
      }
      // If using a Value and Determination, automatically add in an extra critical roll
      if (usingDetermination) {
        if (actor && actor.system.resources) {
          console.log(actor);
          if (actor.system.resources.determination.value < 1) {
            ui.notifications.error("DUNE.roll.noDeterminationToSpend");
            return false;
          }
          actor.update({
            "system.resources.determination.value": actor.system.resources.determination.value - 1
          });
        }
        diceString += '<li class="roll die d20 max">' + 1 + '</li>';
        success += 2;
      }
      // Here we want to check if the success was exactly one (as "1 Successes" doesn't make grammatical sense).
      // We create a string for the Successes.
      let successText = '';
      if (success <= 1) {
        successText = success + ' ' + game.i18n.format('DUNE.roll.success');
      } else {
        successText = success + ' ' + game.i18n.format('DUNE.roll.successPlural');
      }
      let complicationText = '';
      if (complication > 1) {
        const localisedPluralisation = game.i18n.format('DUNE.roll.complicationPlural');
        complicationText = '<h4 class="dice-total failure"> ' + localisedPluralisation.replace('|#|', complication) + '</h4>';
      } else if (complication) {
        complicationText = '<h4 class="dice-total failure"> ' + game.i18n.format('DUNE.roll.complication') + '</h4>';
      }


      // Build a dynamic html using the variables from above.
      const html = `
            <div class="dune roll attribute">
                <div class="dice-roll">
                    <div class="dice-result">
                        <div class="dice-formula">
                            ` + dicePool + `d20
                        </div>
                        <div class="dice-tooltip">
                            <section class="tooltip-part">
                                <div class="parameters">
                                  ` + targetText + `
                                </div>
                                <div class="dice">
                                    <ol class="dice-rolls">` + diceString + `</ol>
                                </div>
                            </section>
                        </div>` +
        complicationText +
        `<h4 class="dice-total">` + successText + `</h4>
                    </div>
                </div>
            </div>
        `;

      // Check if the dice3d module exists (Dice So Nice). If it does, post a roll in that and then
      // send to chat after the roll has finished. If not just send to chat.
      if (game.dice3d) {
        game.dice3d.showForRoll(_r).then((displayed) => {
          this.sendToChat(html, dr, actor, flavorText);
        });
      } else {
        this.sendToChat(html, dr, actor, flavorText);
      };
    });
  }

  async sendToChat(content, roll, actor, flavorText) {
    const rollMode = game.settings.get('core', 'rollMode');
    let conf = {
      user: game.user._id,
      content: content,
      roll: roll,
      sound: 'sounds/dice.wav',
      type: CONST.CHAT_MESSAGE_TYPES.ROLL,
    };
    if (flavorText)
      conf.flavor = flavorText;
    if (actor)
      conf.speaker = ChatMessage.getSpeaker({ actor: actor });
    if (rollMode != 'publicRoll')
      conf = ChatMessage.applyRollMode(conf, rollMode);
    // Send's Chat Message to foundry, if items are missing they will appear as false or undefined and this not be rendered.
    ChatMessage.create(conf).then((msg) => {
      return msg;
    });
  }

  static instance = null;

  static get() {
    if (!DuneRoll.instance)
      DuneRoll.instance = new DuneRoll();
    return DuneRoll.instance;
  }

  // Parse XdYtZfAc || XdYsZfAc
  // {size of dice pool}d{target number}(t|s)[{skill level - for focus}f][{complication range}c][D]
  async parse(cmd, usingDetermination) {
    let actor = game.user.character;
    if (canvas.tokens.controlled.length > 0)
      actor = canvas.tokens.controlled[0].actor;
    let r = cmd.match(/([2-5])d([01]?[0-9])(?:[ts](([4-8])f)?((20|[1][5-9])c)?(D)?)?/);
    let rft = cmd.match(/#\s?(.*)$/)
    if (r) {
      //console.log(r);
      let dicePool = +r[1];
      let target = +r[2];
      let focus = +r[4];
      if (!!r[7]) usingDetermination = true;
      let complicationRange = +r[6];
      this.performTest(dicePool, target, focus, usingDetermination, complicationRange, actor, rft ? rft[1] : null);
    } else
      ui.notifications.error("Unparsable command: " + cmd);
  }

  static previousValues = {
    dicePool: 2
  };

  static rollerTemplate1 = 'systems/dune/templates/apps/roll.html';
  static rollerTemplate2 = 'systems/dune/templates/apps/roll2.html';

  static async ui(forActor) {
    let charData = function () {
      let o = Object.assign({ _template: DuneRoll.rollerTemplate1 }, DuneRoll.previousValues);
      try {
        let actor = forActor ?? game.user.character;
        if (!forActor && canvas.tokens.controlled.length > 0)
          actor = canvas.tokens.controlled[0].actor;
        if (actor && actor.system.Skills && actor.system.Drives) {
          o.determination = actor.system.resources?.determination?.value || 0;
          o.actor = actor.system;
          o._actor = actor;
          o._template = DuneRoll.rollerTemplate2;
        }
      } catch (e) {
        console.log(e);
        return Object.assign({ _template: DuneRoll.rollerTemplate1 }, DuneRoll.previousValues);
      }
      return o;
    };
    let data = charData();
    let html = await renderTemplate(data._template, data);
    let rui = new Dialog({
      title: game.i18n.localize("DUNE.roll.roller"),
      content: html,
      buttons: {
        roll: {
          label: game.i18n.localize('DUNE.apps.rolldice'),
          callback: (html) => {
            let form = html.find('#dice-pool-form');
            if (!form[0].checkValidity()) {
              ui.notifications.error("Invalid Data");
              return false;
            }
            let dicePool, target, focus, usingDetermination, complicationRange, skill = 0, drive = 0;
            form.serializeArray().forEach(e => {
              switch (e.name) {
                case "complicationRange":
                  if (e.value != "")
                    complicationRange = +e.value;
                  break;
                case "dicePoolSlider":
                  DuneRoll.previousValues.dicePool = dicePool = +e.value;
                  break;
                case "targetNumber":
                  DuneRoll.previousValues.target = target = +e.value;
                  break;
                case "skill":
                  skill = +e.value;
                  DuneRoll.previousValues.target = target = drive + skill;
                  break;
                case "drive":
                  drive = +e.value;
                  DuneRoll.previousValues.target = target = drive + skill;
                  break;
                case "usingFocus":
                  if (e.value && +e.value > 1)
                    focus = +e.value;
                  break;
                case "usingFocusOn":
                  if (e.value && e.value == "on")
                    focus = skill;
                  break;
                case "usingDetermination":
                  usingDetermination = e.value == "on";
                  break;
              }
            });
            return DuneRoll.get().performTest(dicePool, target, focus, usingDetermination, complicationRange, data._actor);
          }
        },
        close: {
          label: game.i18n.localize('Close'),
          callback: () => { }
        }
      },
      render: function (h) {
        h.find("#skills-radio input").change(function () {
          let s = $(this).attr("data-skill");
          h.find(".focus-list .hidden").removeClass("show");
          let f = h.find(".focus-list ." + s);
          f.addClass("show");
          if (f.length == 0) {
            h.find(".use-focus input").attr("disabled", "disabled").prop("checked", false);
          } else
            h.find(".use-focus input").attr("disabled", null);
        });
      }
    });
    rui.render(true);
    return rui;
  }

  static selectSkill(rui, skill, focus) {
    rui._element.find(`#skills-radio input[data-skill=${skill}]`).prop("checked", true).change();
    if (focus)
      rui._element.find(`input[name=usingFocusOn]`).prop("checked", true);
  }
}

Handlebars.registerHelper('concat', (...args) => args.slice(0, -1).join(''));
Handlebars.registerHelper('lower', e => e.toLocaleLowerCase());

Hooks.on("chatCommandsReady", function (chatCommands) {
  chatCommands.register({
    name: "/dune",
    aliases: ["/dr"],
    module: "dune",
    callback: (_chatlog, messageText, _chatdata) => {
      DuneRoll.get().parse(messageText);
      return {};
    },
    shouldDisplayToChat: false,
    icon: "<i class='fas fa-dice-d20'></i>",
    description: game.i18n.localize("DUNE.roll.description"),
    autocompleteCallback: (menu, alias, parameters) => {
      // {size of dice pool}d{target number}(t|s)[{skill level - for focus}f][{complication range}c][D]
      const entries = [
        game.chatCommands.createCommandElement(`${alias} 2d12`, "Simple roll with 2 dice, target 12"),
        game.chatCommands.createCommandElement(`${alias} 3d12s8f`, "Roll with 3 dice, target 12, focus to 8"),
        game.chatCommands.createCommandElement(`${alias} 2d12s4c`, "Roll with 2 dice, target 12, complication range 1-4"),
        game.chatCommands.createCommandElement(`${alias} 2d12s8f4c`, "Roll with 2 dice, target 12, focus to 8, complication range 1-4"),
        game.chatCommands.createCommandElement(`${alias} 5d12sD`, "Roll with 5 dice, target 12, using determination"),
        game.chatCommands.createSeparatorElement(),
        game.chatCommands.createInfoElement("{size of dice pool}d{target number}[s[{skill level - for focus}f][{complication range}c][D]]"),
      ];
      entries.length = Math.min(entries.length, menu.maxEntries);
      return entries;
    },
  });
});
