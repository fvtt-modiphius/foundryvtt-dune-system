// -*- js -*-
import ItemSheetDuneTalent from "../../item/sheets/talent.js"
import CompositeActorSheet from "./composite.js"
import * as utls from "../utils.js"
import { DuneRoll } from "../../dice/roll.js"

export default class ActorSheetDuneCharacter extends CompositeActorSheet {

  /** @override */
  get template() {
    if (!game.user.isGM && this.actor.limited)
      return "systems/dune/templates/actors/limited-sheet.html";
    let t;
    switch (this.actor.type) {
      case "Asset": t = "asset"; break;
      case "House": t = "house"; break;
      case "Supporting Character":
        this.npcNoEdit = !game.user.isGM && !this.actor.permission >= foundry.CONST.DOCUMENT_OWNERSHIP_LEVELS.OWNER;
        t = "character";
        break;
      case "Non-Player Character":
        this.npcNoEdit = !game.user.isGM;
      default: t = "character";
    }
    return `systems/dune/templates/actors/${t}-sheet.html`;
  }

  /** @override */
  get isEditable() {
    return super.isEditable && !this.npcNoEdit;
  }

  getData() {
    const data = super.getData();
    data.talentCount = this.actor.items.filter(i => i.type == 'talent').length;
    data.assetCount = this.actor.items.filter(i => i.type == 'asset').length;
    data.editableDrives = this.isEditableDrives();
    return data;
  }

  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      //scrollY: [".tab.main"],
      tabs: [{ navSelector: ".tabs", contentSelector: ".sheet-body", initial: "main" }]
    });
  }

  isEditableDrives() {
    return game.settings.get("dune", "editableDrives") &&
      (!this.actor.isToken || this.token?.actorLink);
  }

  /**
   * Activate event listeners using the prepared sheet HTML
   * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
   * @override
   */
  activateListeners(html) {
    super.activateListeners(html);
    const softLock = this.actor.system.softLock;
    const editable = this.isEditable && !softLock;
    if (editable) {
      html.find(".add-trait").click(this._addTrait.bind(this));
      html.find(".add-asset").click(this._addAsset.bind(this));
      html.find(".trait button").click(this._removeTraitAsset.bind(this));
      html.find(".asset button.del").click(this._removeTraitAsset.bind(this));
      html.find(".talent button.del").click(this._removeTalent.bind(this));
      html.find(".talent button.edit").click(this._editTalent.bind(this));
      if (this.isEditableDrives())
	      html.find(".drive label").dblclick(this._renameDrive.bind(this));
    }
    if (softLock) {
      html.find("input").attr("disabled", true);
      html.find("button").attr("disabled", true);
      html.find(".ability .focus").attr("disabled", false).attr("readonly", true);
    }
    html.find(".ability button.roller").attr("disabled", false).click(this._rollSkill.bind(this));
    html.find(".ability .focus").dblclick(this._rollSkill.bind(this));
    html.find(".talent b").click(e => html.find($(e.target).attr("data-toggle")).toggle());
    html.find(".soft-lock").click(e => this.actor.update({ "system.softLock": !this.actor.system.softLock }));
    if (!editable) // Allow to show note with whole line in non-edit mode
      html.find(".asset").click(e => html.find($(e.target).attr("data-toggle") || $(e.target).parent().attr("data-toggle")).toggle());
    html.find(".asset button.show-note").click(e => html.find($(e.target).attr("data-toggle") || $(e.target).parent().attr("data-toggle")).toggle());

    // allow pictures to grow
    if (html.hasClass("locked")) {
      html.find("header.sheet-header img.profile").click(e => $(e.target).toggleClass("bigger"));
    }
  }

  _addTrait() {
    this.actor.createEmbeddedDocuments("Item",
      [{ type: "trait", name: "trait", system: { temporary: true } }],
      { renderSheet: false });
  }

  _removeTraitAsset(e) {
    let id = $(e.target).attr("data");
    if (!id)
      id = $(e.target).parent().attr("data");
    let item = this.actor.items.get(id);
    if (!item)
      debugger;
    console.log(item, item.system, item.system.temporary);// TODO
    if (item.system.temporary)
      this.actor.deleteEmbeddedDocuments("Item", [id]);
    else
      utls.confirmDelete(() => this.actor.deleteEmbeddedDocuments("Item", [id]), item);
  }

  _addAsset() {
    this.actor.createEmbeddedDocuments("Item",
      [{ type: "asset", name: "asset", system: { temporary: true, quality: 0 } }],
      { renderSheet: false });
  }

  _removeTalent(e) {
    let id = $(e.target).attr("data");
    if (!id)
      id = $(e.target).parent().attr("data");
    let item = this.actor.items.get(id);
    utls.confirmDelete(() => this.actor.deleteEmbeddedDocuments("Item", [id]), item);
  }

  _editTalent(e) {
    let id = $(e.target).attr("data");
    if (!id)
      id = $(e.target).parent().attr("data");
    let item = this.actor.items.get(id);
    new ItemSheetDuneTalent(item).render(true);
  }

  _rollSkill(e) {
    let id = $(e.target).parent().attr("data-ability");
    if (!id)
      id = $(e.target).parent().parent().attr("data-ability");
    const focus = $(e.target).hasClass('focus') && !!$(e.target).val()?.trim();
    console.log(id, focus, ui);
    DuneRoll.ui(this.actor).then(ui => {
      const config = () => {
        if (ui.rendered) DuneRoll.selectSkill(ui, id, focus);
        else setTimeout(config, 100);
      }
      config();
    });
  }

  _removeDrive(id) {
    const update = {};
    update[`system.Drives.-=${id}`] = null;
    this.actor.update(update);
  }

  _renameDrive(e) {
    const me = this;
    const id = $(e.target).parent().attr("data-ability");
    const old = this.actor.system.Drives[id].value;
    const drives = Object.keys(this.actor.system.Drives);
    const suggest = '<datalist id="drives-list">' + utls.listDrives().filter(d => !drives.includes(d)).map(x => `<option value="${x}">`).join('') + '</datalist>';
    console.log(suggest);
    new Dialog({
      title: `Replace Drive ${id}`,
      content: '<input id="driveName" list="drives-list" type="text" value="" required />' + suggest,
      default: "create",
      buttons: {
	create: {
	  icon: '<i class="fas fa-check"></i>',
	  label: "Replace",
	  callback: (html) => {
	    const val = html.find("input#driveName").val();
	    console.log(`New drive: ${val}`);
	    if (val && val != '') {
	      const update = {};
	      update[`system.Drives.-=${id}`] = null;
	      update[`system.Drives.${val}`] = {
		value: old,
		min: 4,
		max: 8,
		statement: null,
		focus: ''
	      };
	      me.actor.update(update);
	    } else
	      throw new Error('You must submit a Drive name')
	  }
	},
	cancel: {
	  icon: '<i class="fas fa-close"></i>',
	  label: "Cancel"
	}
      }
    }).render(true);
  }
}

Hooks.on("setup", function () {
  game.settings.register("dune", "editableDrives", {
    name: "Editable Drives",
    scope: 'world',
    config: true,
    type: Boolean,
    default: false,
    hint: game.i18n.localize('DUNE.edit_drives_hint')
  });
});
