export default class DuneCharacter {

  static migrate(act) {
    console.log(act);
    if (act.system.Traits) {
      if (act.system.Traits.length) {
        for (let trait of act.system.Traits) {
          console.log(trait);
          act.createEmbeddedDocuments("Item",
            [{
              type: "trait", name: trait.name,
              system: { temporary: trait.temporary }
            }],
            { renderSheet: false });
        }
        act.update({ "system.Traits": null });
      }
      else if (act.system.Traits['[0]']) {
        for (let i in act.system.Traits) {
          let trait = act.system.Traits[i];
          console.log(trait);
          act.createEmbeddedDocuments("Item",
            [{
              type: "trait", name: trait.name,
              system: { temporary: trait.temporary }
            }],
            { renderSheet: false });
        }
        act.update({ "system.Traits": null });
      }
    }
    if (act.system.Assets) {
      if (act.system.Assets.length) {
        for (let asset of act.system.Assets) {
          console.log(asset);
          act.createEmbeddedDocuments("Item",
            [{
              type: "asset", name: asset.name,
              system: { temporary: asset.temporary, quality: asset.quality }
            }],
            { renderSheet: false });
        }
        act.update({ "system.Assets": null });
      }
      else if (act.system.Assets['[0]']) {
        for (let i in act.system.Assets) {
          let asset = act.system.Assets[i];
          console.log(asset);
          act.createEmbeddedDocuments("Item",
            [{
              type: "asset", name: asset.name,
              system: { temporary: asset.temporary, quality: asset.quality }
            }],
            { renderSheet: false });
        }
        act.update({ "system.Assets": null });
      }
    }
    if (act.system.Skills.Battle.min == 4 && act.system.Skills.Battle.max == 8) {
      console.log("Migrate skills");
      let up = {};
      for (let skill in act.system.Skills) {
        up["system.Skills." + skill + ".min"] = 0;
        up["system.Skills." + skill + ".max"] = act.system.Skills[skill].value;
      }
      //console.log(up);
      act.update(up);
    }
  }

  static heal(act) {
    if (act == null) {
      if (canvas.tokens.controlled.length > 0)
        act = canvas.tokens.controlled[0].actor;
      else
        act = game.user.character;
      if (act == null)
        return "ERROR: You must pass a character, or select a token";
    }
    if (typeof act == "string")
      this.heal(game.actors.get(act));
    else {
      let up = {};
      for (let skill in act.system.Skills) {
        if (act.system.Skills[skill].value != act.system.Skills[skill].max)
          up["system.Skills." + skill + ".value"] = act.system.Skills[skill].max;
      }
      act.update(up);
    }
  }

  static healAll() {
    for (let act of game.actors.entries) {
      switch (act.data.type) {
        case "Player Character":
        case "Supporting Character":
        case "Non-Player Character":
          this.heal(act);
          break;
      }
    }
  }
}

globalThis.DuneCharacter = DuneCharacter;
