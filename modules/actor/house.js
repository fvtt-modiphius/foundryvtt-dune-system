export default class DuneHouse {

  static migrate(house) {
    for (const item of house.items.values()) {
      if (item.type === "enemy") {
        if (item.system.hatered) {
          item.update({"system.hatred": item.system.hatered, "system.hatered": null});
          console.log(`+++++ migrated ${item.name} +++++`, item);
        }
      }
    }
  }
}
