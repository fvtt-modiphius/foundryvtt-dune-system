export function confirmDelete(fn, entity) {
  let t = game.i18n.format("DUNE.ui.confirmDelete", {what: game.i18n.localize('TYPES.Item.' + entity.type)});
  let c = game.i18n.format("DUNE.ui.areYouSureDelete", {what: entity.name});
  switch (entity.type) {
    case "domain":
      let domain = game.i18n.localize(entity.system.primary ? 'DUNE.primary' : 'DUNE.secondary');
      if (entity.system.area)
        domain += " " + entity.system.area;
      if (entity.system.field)
        domain += " (" + entity.system.field + ")";
      if (entity.system.details)
        domain += " -- " + entity.system.details;
      c = game.i18n.format("DUNE.ui.areYouSureDelete", {what: domain});
      break;
  }
  let d = new Dialog({
    title: t,
    content: c,
    buttons: {
      "delete": {
        icon: '<i class="fas fa-trash"></i>',
        label: game.i18n.localize('Delete'),
        callback: fn
      },
      cancel: {
        icon: '<i class="fas fa-times"></i>',
        label: game.i18n.localize('Cancel')
      }
    },
    default: "cancel"
  });
  d.render(true);
}

export function itemFromArray(data, id) {
  if (data.splice) {
    // is array
    let idx = id.substr(1, id.length - 2);
    return data[+idx];
  }
  else {
    // is ~Map
    return data[id];
  }
}

export function confirmDeleteFromArray(cb, data, id, item) {
  confirmDelete(() => {
    if (data.splice) {
      // is array
      let idx = id.substr(1, id.length - 2);
      data.splice(+idx, 1);
    }
    else {
      // is ~Map
      delete data[id];
      data = Object.values(data);
    }
    cb.apply(null, [data]);
  }, item || itemFromArray(data, id))
}

let __driveList = null;
export function listDrives() {
  if (!__driveList) {
    const std = Object.keys(game.data.model.Actor['Player Character'].Drives);
    const pp = Object.keys(game.data.model.Actor['Player Character'].PPDrives);
    __driveList = std.concat(pp);
  }
  return __driveList;
}
