# Dune: The Roleplaying Game (2d20) for FoundryVTT

Community contributed and maintained system for playing Dune 2D20 system with FoundryVTT (https://foundryvtt.com/).

Unofficial but approved by Modiphius

## Licence

- Content: [Dune Roleplaying Game](https://www.modiphius.net/pages/discover-dune-roleplaying-game) © Modiphius Entertainment.
- Foundry VTT: [Limited License Agreement for module development](https://foundryvtt.com/article/license/).
- Source code: Copyright © 2021-2024 Bastien Durel, licenced under the [GPL 3.0](https://git.geekwu.org/dune/foundryvtt-dune-system/-/blob/master/LICENSE).

All copyright assets used with explicit consent from Modiphius Entertainment. The fvtt-modiphius developer community holds no claim to underlying copyrighted assets.

The 2d20 system and Modiphius Logos are copyright Modiphius Entertainment Ltd. 2015–2021.

All 2d20 system text is copyright Modiphius Entertainment Ltd.

## using dice roller in chat and macros

{size of dice pool}d{target number}(t|s)[{skill level - for focus}f][{complication range}c][D]

example: 
> /dr 4d12t6f18c
>
>  roll 4 dice, with a target of 12 or below. Anything under or equal to a 6 counts as a focus based crit and anything equal to or more than 18 is a complication. Optionally you can add a "D" to the end of the string representing the character spending a point of determination.

## Translation of compendiums

Compendiums could be translated using [babele](https://gitlab.com/riccisi/foundryvtt-babele) module.

If babele is present and activated, Dune module will load translation files from `packs/translations/[lang]` directory.

Inclusion of compendium translation into distribution would require translation publisher's agreement, please make sure to get it before trying to contribute any.
