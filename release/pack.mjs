
import { compilePack } from "@foundryvtt/foundryvtt-cli";
import { promises as fs } from 'fs';

const MODULE_ID = 'dune';
const yaml = false;
const target = process.argv[2] || '_build';

const packs = await fs.readdir('./packs');
for (const pack of packs) {
    if (pack.match(/\.db$/)) continue;
    console.log('Packing ' + pack);
    await compilePack(
        `packs/${pack}/_source`,
        `${target}/packs/${pack}`,
        { yaml }
    );
}
