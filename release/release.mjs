//import fetch from "node-fetch";	// Import the fetch module

function parseBoolean(string, defaultValue = null) {
  if (typeof string !== "string") {
    throw new TypeError("Input must be a string");
  }

  switch (string.trim().toLowerCase()) {
    case "true": return true;
    case "false": return false;
    case "0": return false;
    case "1": return true;
    default:
      if (/^-?\d+$/.test(string)) return !!+string;
      return defaultValue;
  }
};
const body = {
  "id": process.env.FVTT_PACKAGE_ID,
  "dry-run": true,
  "release": {
    "version": process.env.FVTT_PACKAGE_VERSION || process.env.CI_COMMIT_TAG.replace(/^v/, ''),
    "manifest": process.env.FVTT_MANIFEST_URL,
    "notes": process.env.FVTT_CHANGELOG_URL,
    "compatibility": {
      "minimum": process.env.FVTT_MINIMUM_CORE_VERSION,
      "verified": process.env.FVTT_VERIFIED_CORE_VERSION || process.env.FVTT_COMPATIBLE_CORE_VERSION,
    }
  }
}
if (process.env.FVTT_MAXIMUM_CORE_VERSION) {
  body.release.compatibility.maximum = process.env.FVTT_MAXIMUM_CORE_VERSION
}
if (process.env.FVTT_DRY_RUN) {
  body["dry-run"] = parseBoolean(process.env.FVTT_DRY_RUN)
}
console.log(body)
const config = {
  headers: {
    'Content-Type': 'application/json',
    'Authorization': process.env.FVTT_PASSWORD || '--'
  },
  method: "POST",
  body: JSON.stringify(body)
}
const response = await fetch("https://api.foundryvtt.com/_api/packages/release_version/", config);
const response_data = await response.json()
console.log(response_data)
